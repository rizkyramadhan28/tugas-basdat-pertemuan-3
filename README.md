## Cara Install

Berikut adalah cara installasi dan penggunaan aplikasi ini.

-   Install [Composer](https://getcomposer.org/)
-   Lakukan clone pada aplikasi ini
-   Buka folder, lalu lakukan 'composer install'
-   cp .env.example .env
-   Set up database
-   php artisan key:generate
-   php artisan migrate
-   Jalankan aplikasi dengan menggunakan 'php artisan serve'
