<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Tugas Basis Data Multimedia">
    <meta name="author" content="Rizky Ramadhan">

    @yield("title")

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('template/bootstrap.min.css') }}" rel="stylesheet">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

</head>

<body>
    <main>
        <div class="container py-4">
            <header class="pb-3 mb-4 border-bottom">
                <a href="{{ route('dashboard.book.index') }}"
                    class="d-flex align-items-center text-dark text-decoration-none">
                    <span class="fs-4"><b>Rizky Ramadhan (17102044)</b></span>
                </a>
            </header>

            <div class="p-5 mb-4 bg-light rounded-3">
                <div class="container-fluid py-1">
                    <h1 class="display-5 fw-bold">Daftar Buku</h1>
                    <p class="col-md-8 fs-4">Fitur: menambahkan, mengurangi, mengedit dan menghapus buku.</p>
                </div>
            </div>

            @yield("content")

            <footer class="pt-3 mt-4 text-muted border-top">
                &copy; Rizky Ramadhan - 2021
            </footer>
        </div>
    </main>

    <script src="{{ asset('template/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('template/bootstrap.min.js') }}"></script>

</body>

</html>