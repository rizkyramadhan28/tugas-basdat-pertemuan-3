@extends("template")

@section("title")
<title>Rizky Ramadhan (17102044) - Index</title>
@endsection

@section("content")
@if(session()->has("message_success"))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session()->get("message_success") }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

<div class="row align-items-md-stretch">
    <div class="col-md-12">
        <div class="h-100 p-5 border rounded-3">
            <a href="{{ route('dashboard.book.create') }}" class="btn btn-primary btn-sm">Tambah Buku</a>

            <hr>

            <table class="table table-hover" style="width: 100%;">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Foto</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Deskripsi</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($books as $item)
                    <tr>
                        <th>{{ $loop->iteration }}</th>
                        <td>
                            <img src="{{ url('/images/' . $item['image']) }}" class="img-fluid border"
                                style="width: 10rem;">
                        </td>
                        <td><b>{{ $item["name"] }}</b></td>
                        <td>{{ $item["description"] }}</td>
                        <td style="width: 15%;">
                            <form action="{{ route('dashboard.book.delete', $item) }}" method="POST">
                                @csrf
                                @method("DELETE")

                                <a href="{{ route('dashboard.book.edit', $item) }}"
                                    class="btn btn-warning btn-sm">Edit</a>
                                <button class="btn btn-danger btn-sm">Hapus</button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5" style="text-align: center">Tidak ada data ...</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection