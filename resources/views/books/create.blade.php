@extends("template")

@section("title")
<title>Rizky Ramadhan (17102044) - Create</title>
@endsection

@section("content")
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if(session()->has("message_error"))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    {{ session()->get("message_error") }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif

<div class="row align-items-md-stretch">
    <div class="col-md-12">
        <div class="h-100 p-5 border rounded-3">
            <form action="{{ route('dashboard.book.store') }}" method="POST" enctype="multipart/form-data">
                @csrf

                <div class="form-group mb-2">
                    <label>Nama</label>
                    <input name="name" type="text" class="form-control" placeholder="Masukkan nama buku">
                </div>

                <div class="form-group mb-2">
                    <label>Deskripsi</label>
                    <textarea name="description" class="form-control" rows="3"
                        placeholder="Masukkan deskripsi buku"></textarea>
                </div>

                <div class="form-group mb-4">
                    <label>Foto</label>
                    <input name="image" type="file" class="form-control" placeholder="Masukkan foto">
                </div>

                <button type="submit" class="btn btn-primary btn-sm float-right">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection