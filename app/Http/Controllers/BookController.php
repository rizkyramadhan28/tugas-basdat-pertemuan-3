<?php

namespace App\Http\Controllers;

use File;
use App\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();

        return view("books.index", ["books" => $books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("books.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            "name" => "required",
            "description" => "required",
            "image" => "required|file|image|max:1024|mimes:jpeg,png"
        ]);

        if($request->hasFile("image") && $request->file("image")->isValid()) {
            $filename = time() . "." . request()->image->getClientOriginalExtension();
            request()->image->move(public_path("images"), $filename);

            $book = new Book;

            $book->name = $validateData["name"];
            $book->description = $validateData["description"];
            $book->image = $filename;

            $book->save();

            return redirect(route("dashboard.book.index"))->with(["message_success" => "Sukses"]);
        } 

        return redirect(route("dashboard.book.create"))->with(["message_error" => "Error"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        return view("books.edit", [
            "book" => $book
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $validateData = $request->validate([
            "name" => "required",
            "description" => "required",
            "image" => "required|file|image|max:1024|mimes:jpeg,png"
        ]);

        if($request->hasFile("image") && $request->file("image")->isValid()) {
            File::delete(public_path() . "/images/" . $book->image);

            $filename = time() . "." . request()->image->getClientOriginalExtension();
            request()->image->move(public_path("images"), $filename);

            $book->name = $validateData["name"];
            $book->description = $validateData["description"];
            $book->image = $filename;

            $book->save();

            return redirect(route("dashboard.book.index"))->with(["message_success" => "Sukses"]);
        } 

        return redirect(route("dashboard.book.create"))->with(["message_error" => "Error"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        File::delete(public_path() . "/images/" . $book->image);

        $book->delete();

        return redirect(route("dashboard.book.index"))->with(["message_success" => "Sukses"]);
    }
}