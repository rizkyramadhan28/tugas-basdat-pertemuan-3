<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", "BookController@index")->name("dashboard.book.index");
Route::get("/create", "BookController@create")->name("dashboard.book.create");
Route::post("/store", "BookController@store")->name("dashboard.book.store");
Route::get("/edit/{book}", "BookController@edit")->name("dashboard.book.edit");
Route::put("/update/{book}", "BookController@update")->name("dashboard.book.update");
Route::delete("/delete/{book}", "BookController@destroy")->name("dashboard.book.delete");